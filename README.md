# Django Todo

A todo app written with Django by __APPLICANT__.

Any questions during development should be directed to <matthew.downey@webit.com.au>.

## Features

* Create new items
* Mark one/all item(s) as complete/incomplete
* View complete/incomplete/all items
* Delete items

## Setup

    __APPLICANT_TODO__

## Running

    __APPLICANT_TODO__
